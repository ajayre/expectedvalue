package com.wisercare.expectedvalue;

import java.util.Comparator;

public class TreatmentExpectedValueComparator  implements Comparator<TreatmentExpectedValue> {

    @Override
    public int compare(TreatmentExpectedValue value1, TreatmentExpectedValue value2) {
        return value2.getExpectedValue().compareTo(value1.getExpectedValue());
    }
}
